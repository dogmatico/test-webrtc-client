import {Component} from 'angular2/core';
import {SignalingService} from './services/signalingService';
import {webrtc} from './services/webrtc';

@Component({
    selector: 'my-app',
    template: ` <h1>Test WebRTC</h1>
                <div *ngIf="!localStream" class="no-video">
                  No stream
                </div>
                <video *ngIf="localStream" class="test-video" [src]="localStream" autoplay></video>
                <div>
                  <button (click)="startCapture()">Start</button>
                </div>`,
    styles : [` .test-video {
                  width: 275px;
                  height: 275px;
                }
                .no-video {
                  width: 275px;
                  height: 275px;
                  font-size: 30px;
                }
    `],
    providers : [SignalingService]
})
export class AppComponent {
  public localStream : String = null;
  private _videoStream;
  private _peerConnect;
  private _dataChannel;


  constructor (private io : SignalingService) {
  }

  private createOffer() {
    this._peerConnect.createOffer(offer => {
      this._peerConnect.setLocalDescription(new webrtc.RTCSessionDescription(offer), () => {
        console.log(offer);
        this.io.socket.emit('newOffer', offer);
      }, err => console.log(err));
    }, err => console.log(err));
  }

  private acceptAnswer(answer) {
    console.log('Received Answer');
    this._peerConnect.setRemoteDescription(new webrtc.RTCSessionDescription(answer), () => {
      console.log('Set remote description');
    }, err => console.log(err));
  }

  private configDataChannel(dataChannel) {
    dataChannel.onopen = function() {
      console.log("pc1: data channel open");
      dataChannel.onmessage = function(event) {
        var data = event.data;
        console.log("dc1: received '" + data + "'");
      };
      console.log("dc1: sending 'pong'");
      dataChannel.send('ping');
    };
  }

  startCapture () {
    navigator.mediaDevices.getUserMedia({
        audio : true
      })
      .then(mediaStream => {
        this._videoStream = mediaStream;

        this._peerConnect = new webrtc.RTCPeerConnection();
        this._peerConnect.onicecandidate = (e) => {
          console.log(e);
          if(!e.candidate) return;
          this._peerConnect.addIceCandidate(e.candidate);
          this.io.socket.emit('ICECandidate', e.candidate);
        };
        //this._peerConnect.addStream(mediaStream);
        this._dataChannel = this._peerConnect.createDataChannel("myLabel");
        this.configDataChannel(this._dataChannel);
        this.createOffer();
        this.io.socket.on('newResponse', this.acceptAnswer.bind(this));


        this.localStream = window.URL.createObjectURL(this._videoStream);

      })
      .catch(err => console.log(err));
  }

}
