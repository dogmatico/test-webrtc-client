import {Injectable} from 'angular2/core';
import * as io from '../../node_modules/socket.io-client/socket.io.js';

console.log(io);

@Injectable()
export class SignalingService {
  public salute : String;

  public socket = io.connect('http://localhost:10003');

  constructor() {

    this.salute = 'lol';
  }
}
